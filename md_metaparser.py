from lxml.html import etree, parse, tostring, open_in_browser, fromstring
from md_candidato import madiva_candidato

class madiva_metaparser:

    def __init__(self):
        self.url_base = ""
        self.patron_base = ""
        self.nombre_fichero = ""

    def extrae_fullpath(self, nodo,hijo=None):
        result = ""

        padre = nodo.find('..')
        if padre is not None:
            result += self.extrae_fullpath(padre, nodo)
        else:
            result += ""
        classname = nodo.get("class")
        str = nodo.tag
        if classname:
            str += '_' + classname
        result += "/" + str


        #if hijo is not None:
        #    for h in nodo:
        #        if h==hijo:
        #            break
        #        result += "<" + h.tag + "/>"
        return result

    def inicia(self, n_fichero, u_base):
        self.nombre_fichero = n_fichero
        self.url_base = u_base
        self.patron_base = self.extrae_patron(n_fichero, u_base)
        if self.patron_base!='':
            print 'Encontrado patron base:' + self.patron_base
            return True
        else:
            return False

    def extrae_patron(self, nombrefichero, cadena):
        parser = etree.HTMLParser()
        tree = etree.parse(nombrefichero, parser)
        doc = tree.getroot()
        for element in doc.iter():
            href = element.get('href')
            if href and href.find(cadena) > -1:
                return self.extrae_fullpath(element)
        return ''

    def procesa(self, nombrefichero):
        parser = etree.HTMLParser()
        tree = etree.parse(nombrefichero, parser)
        doc = tree.getroot()
        lista = []
        #print (tostring(doc))
        #e = doc.xpath(".//a[contains(text(),"+cadena+")]")
        for element in doc.iter():
            href = element.get('href')
            if href and not any(elem.url==href for elem in lista):
                    c = madiva_candidato()
                    c.patron = self.extrae_fullpath(element)
                    c.url = href
                    lista.append(c)

        #ahora la imprimimos ordenadamente
        lista.sort(key=lambda x: x.T(1,1, self.url_base, self.patron_base))
        for c in lista:
            print str(c.T(1,1, self.url_base, self.patron_base)) + "->" + c.url



    def printinfoctrl(self):
        print "U Y P:"
        print self.url_base
        print self.patron_base
