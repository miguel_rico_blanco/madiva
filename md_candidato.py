import re

class madiva_candidato:
    def __init__(self):
        self._url=''
        self._patron = ''
        self._penalizacion = 1

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, value):
        self._url = value

    @property
    def patron(self):
        return self._patron

    @patron.setter
    def patron(self, value):
        self._patron = value

    def T(self,alfa, beta, url_base, patron_base):
        return  alfa*self.Comp(url_base, self.url) + beta*self.Comp(patron_base, self.patron)

    def Comp(self, cad1, cad2):
        #print "Comparando:" +  cad1 + " con " + cad2
        total = 0
        p1 = re.split("/&?",cad1)
        p2 = re.split("/&?",cad2)

        l1 =  len(p1) - 1
        l2 = len(p2) - 1

        if l2 > l1:
            l1, l2 = l2, l1

        for i1 in range(0,l1):
            if i1>l2:
                total += (l1-i1)*self._penalizacion
            else:
                if p1[i1] != p2[i1]:
                    total += (l1-i1)*self._penalizacion
                    #penalizacion
        return total
