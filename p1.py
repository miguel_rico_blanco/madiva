#!/usr/bin/python
import sys
from md_metaparser import madiva_metaparser
import os.path
print "Comenzando"

argumentos = len(sys.argv)
if argumentos==3:
    nombre_fichero = sys.argv[1]
    cadena = sys.argv[2]

    if os.path.exists(nombre_fichero) and os.path.isfile(nombre_fichero):
            d = madiva_metaparser()
            d.inicia(nombre_fichero, cadena)
            d.procesa(nombre_fichero)

            d.printinfoctrl()
    else:
        print "El fichero no existe"
else:
    print "Uso python p1.py <nombrefichero> <urlabuscar>"
print "finalizando"
